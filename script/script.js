channelKeys = [49, 50, 51, 52];
previousKeys = [37,40];
nextKeys = [38,39];
dnews = [
    'P4hwOUfb-hs',
    'vAgXA1mEHFU',
    'NIz42cy0vLQ',
    'eXZGIoZokJE',
    'aIUu5uMKB70',
    'yyOg_KipUAo',
    'SrJVydq4KA4',
    '1HsuhS2XdY4',
    'yB8CBEFDNC8',
    'tClZGWlRLoE',
    'iEurFZYI70Y',
    'lXK244l9o30',
    '6ZuX-gr4LoU',
    'K5UzBOiuqec',
    'kEQNA13yVIM',
    'Phz-y0j8k1M',
    '_KEso-7eMVc',
    'hyd80b2iDV4',
    'e9wiM_mUbXU',
    'QvKPAz23y-Q',
    'DNILir-SvfM',
    'H9lc-LZ_7dg',
    'BsWDF8lIGFs'
];

var tv;
var clip;
var clipState = false;

var channels = ["http://iptv.atnog.org/hls/rtp1.m3u8",
    "http://iptv.atnog.org/hls/rtp2.m3u8",
    "http://iptv.atnog.org/hls/sic.m3u8",
    "http://iptv.atnog.org/hls/tvi.m3u8"];

channel = 0;

$(document).ready(function(){
    $( document ).keydown(function( event ) {
        if(!clipState) {
            if (channelKeys.indexOf(event.which) >= 0) {
                event.preventDefault(event.which);
                startChannel(event.which - 49);
            }

            if (previousKeys.indexOf(event.which) >= 0) {
                event.preventDefault(event.which);
                previousChannel();
            }

            if (nextKeys.indexOf(event.which) >= 0) {
                event.preventDefault(event.which);
                nextChannel();
            }
            if (event.which == 32) {
                event.preventDefault(event.which);
                callAPI();
            }
        }
    });

    startChannel(0);
});


function startChannel(index) {
    if(index>= 0 && index < channelKeys.length) {
        channel = index;
    }

    $('#tv').html(' ');
    tv = null;
    tv = new Clappr.Player({
        source: channels[channel],
        parentId: "#tv",
        disableKeyboardShortcuts: true,
        allowUserInteraction: false,
        autoPlay: true,
        mediacontrol: {seekbar: "transparent", buttons: "transparent"}
    });
}

function nextChannel() {
    if(channel+1 < channelKeys.length) {
        startChannel(++channel);
    }
}


function previousChannel() {
    if(channel-1 >= 0) {
        startChannel(--channel);
    }
}



function callAPI(id){
    clipState = true;
    id = id>=0?id:parseInt(Math.random()*dnews.length);
    console.log(id);
    clip.onYouTubeIframeAPIReady(dnews[id]);
}


clip = new youtube();