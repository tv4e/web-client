class youtube {

    constructor(){
        this.tag = document.createElement('script');
        this.tag.src = "https://www.youtube.com/iframe_api";
        this.firstScriptTag = document.getElementsByTagName('script')[0];
        this.firstScriptTag.parentNode.insertBefore(this.tag, this.firstScriptTag);
        this.player = null;

    }

    onYouTubeIframeAPIReady(id) {
        $('#clip').remove();
        $('body').append('<div id="clip"></div>');
        var id = id?id:'5DEdR5lqnDE';
        this.player = new YT.Player('clip', {
            videoId: id,
            playerVars: { 'autoplay': 1, 'controls': 0, 'showinfo': 0},
            events: {
                'onReady': this.onPlayerReady,
                'onStateChange': this.onPlayerStateChange
            }
        });
    }

    onPlayerReady(event) {
        tv.pause();
        event.target.playVideo();
    }

    onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.ENDED) {
            $('#clip').remove();
            $('body').append('<div id="clip"></div>');
            tv.play();
            clipState = false;
        }
    }

}